package main;

public class SortingArray {
    int swapCounter = 0;

    public int sort(int[] inputArray) {

        for (int i = 0; i < inputArray.length - 1; i++) {
            int minLocation = i;
            for (int j = i; j < inputArray.length; j++) {
                if (inputArray[j] < inputArray[minLocation]) {
                    minLocation = j;
                }
            }
            if (minLocation != i) {
                swap(minLocation, i, inputArray);
            }
        }
        return swapCounter;
    }

    public void swap(int minLocation, int i, int[] inputArray) {
        int temporary;
        temporary = inputArray[minLocation];
        inputArray[minLocation] = inputArray[i];
        inputArray[i] = temporary;
        swapCounter += 2;
    }

}
