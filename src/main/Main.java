package main;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        SortingArray sortingArray = new SortingArray();
        System.out.print("please enter length of array :");
        Scanner userInput = new Scanner(System.in);
        int length = Integer.parseInt(userInput.nextLine());
        if (length < 1 || length > 300000) {
            System.out.print("please enter length of array in the appropriate range:");
            length = Integer.parseInt(userInput.nextLine());
        }
        int inputArray[] = new int[length];
        for (int input = 0; input < length; input++) {
            System.out.print(String.format("please enter value of index %d :", input + 1));
            inputArray[input] = Integer.parseInt(userInput.nextLine());
        }


        int swapCounter = sortingArray.sort(inputArray);
        System.out.println("number of indexes that are swapped after sorting : " + swapCounter);
    }

}

